package com.safiot.common.enums;

/**
 * 操作状态
 * 
 * @author safiot
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
