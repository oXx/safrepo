package com.safiot.passive.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.safiot.common.base.BaseEntity;

/**
 * 锁具表 passive_lock
 * 
 * @author safiot
 * @date 2019-01-09
 */
public class Lock extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/** 锁编号 */
	private Integer lockId;
	/** 锁编号 */
	private String lockSn;
	/** 锁名 */
	private String lockName;
	/**  */
	private Integer lockUserid;
	/**  */
	private String lockRemark;
	/**  */
	private String lockStatus;
	/**  */
	private Integer lockMac;

	public void setLockId(Integer lockId) 
	{
		this.lockId = lockId;
	}

	public Integer getLockId() 
	{
		return lockId;
	}
	public void setLockSn(String lockSn) 
	{
		this.lockSn = lockSn;
	}

	public String getLockSn() 
	{
		return lockSn;
	}
	public void setLockName(String lockName) 
	{
		this.lockName = lockName;
	}

	public String getLockName() 
	{
		return lockName;
	}
	public void setLockUserid(Integer lockUserid) 
	{
		this.lockUserid = lockUserid;
	}

	public Integer getLockUserid() 
	{
		return lockUserid;
	}
	public void setLockRemark(String lockRemark) 
	{
		this.lockRemark = lockRemark;
	}

	public String getLockRemark() 
	{
		return lockRemark;
	}
	public void setLockStatus(String lockStatus) 
	{
		this.lockStatus = lockStatus;
	}

	public String getLockStatus() 
	{
		return lockStatus;
	}
	public void setLockMac(Integer lockMac) 
	{
		this.lockMac = lockMac;
	}

	public Integer getLockMac() 
	{
		return lockMac;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("lockId", getLockId())
            .append("lockSn", getLockSn())
            .append("lockName", getLockName())
            .append("lockUserid", getLockUserid())
            .append("lockRemark", getLockRemark())
            .append("lockStatus", getLockStatus())
            .append("lockMac", getLockMac())
            .toString();
    }
}
