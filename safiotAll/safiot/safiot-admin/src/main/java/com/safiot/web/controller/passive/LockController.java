package com.safiot.web.controller.passive;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.safiot.common.annotation.Log;
import com.safiot.common.enums.BusinessType;
import com.safiot.system.domain.Lock;
import com.safiot.system.service.ILockService;
import com.safiot.framework.web.base.BaseController;
import com.safiot.common.page.TableDataInfo;
import com.safiot.common.base.AjaxResult;
import com.safiot.common.utils.ExcelUtil;

/**
 * 锁具 信息操作处理
 * 
 * @author safiot
 * @date 2019-01-09
 */
@Controller
@RequestMapping("/system/lock")
public class LockController extends BaseController
{
    private String prefix = "system/lock";
	
	@Autowired
	private ILockService lockService;
	
	@RequiresPermissions("system:lock:view")
	@GetMapping()
	public String lock()
	{
	    return prefix + "/lock";
	}
	
	/**
	 * 查询锁具列表
	 */
	@RequiresPermissions("system:lock:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(Lock lock)
	{
		startPage();
        List<Lock> list = lockService.selectLockList(lock);
		return getDataTable(list);
	}
	
	
	/**
	 * 导出锁具列表
	 */
	@RequiresPermissions("system:lock:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Lock lock)
    {
    	List<Lock> list = lockService.selectLockList(lock);
        ExcelUtil<Lock> util = new ExcelUtil<Lock>(Lock.class);
        return util.exportExcel(list, "lock");
    }
	
	/**
	 * 新增锁具
	 */
	@GetMapping("/add")
	public String add()
	{
	    return prefix + "/add";
	}
	
	/**
	 * 新增保存锁具
	 */
	@RequiresPermissions("system:lock:add")
	@Log(title = "锁具", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(Lock lock)
	{		
		return toAjax(lockService.insertLock(lock));
	}

	/**
	 * 修改锁具
	 */
	@GetMapping("/edit/{lockId}")
	public String edit(@PathVariable("lockId") Integer lockId, ModelMap mmap)
	{
		Lock lock = lockService.selectLockById(lockId);
		mmap.put("lock", lock);
	    return prefix + "/edit";
	}
	
	/**
	 * 修改保存锁具
	 */
	@RequiresPermissions("system:lock:edit")
	@Log(title = "锁具", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(Lock lock)
	{		
		return toAjax(lockService.updateLock(lock));
	}
	
	/**
	 * 删除锁具
	 */
	@RequiresPermissions("system:lock:remove")
	@Log(title = "锁具", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public AjaxResult remove(String ids)
	{		
		return toAjax(lockService.deleteLockByIds(ids));
	}
	
}
