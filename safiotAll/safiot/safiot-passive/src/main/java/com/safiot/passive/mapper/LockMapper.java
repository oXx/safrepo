package com.safiot.passive.mapper;

import com.safiot.system.domain.Lock;
import java.util.List;	

/**
 * 锁具 数据层
 * 
 * @author safiot
 * @date 2019-01-09
 */
public interface LockMapper 
{
	/**
     * 查询锁具信息
     * 
     * @param lockId 锁具ID
     * @return 锁具信息
     */
	public Lock selectLockById(Integer lockId);
	
	/**
     * 查询锁具列表
     * 
     * @param lock 锁具信息
     * @return 锁具集合
     */
	public List<Lock> selectLockList(Lock lock);
	
	/**
     * 新增锁具
     * 
     * @param lock 锁具信息
     * @return 结果
     */
	public int insertLock(Lock lock);
	
	/**
     * 修改锁具
     * 
     * @param lock 锁具信息
     * @return 结果
     */
	public int updateLock(Lock lock);
	
	/**
     * 删除锁具
     * 
     * @param lockId 锁具ID
     * @return 结果
     */
	public int deleteLockById(Integer lockId);
	
	/**
     * 批量删除锁具
     * 
     * @param lockIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteLockByIds(String[] lockIds);
	
}