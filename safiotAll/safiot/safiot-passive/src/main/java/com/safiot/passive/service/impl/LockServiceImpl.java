package com.safiot.passive.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.safiot.system.mapper.LockMapper;
import com.safiot.system.domain.Lock;
import com.safiot.system.service.ILockService;
import com.safiot.common.support.Convert;

/**
 * 锁具 服务层实现
 * 
 * @author safiot
 * @date 2019-01-09
 */
@Service
public class LockServiceImpl implements ILockService 
{
	@Autowired
	private LockMapper lockMapper;

	/**
     * 查询锁具信息
     * 
     * @param lockId 锁具ID
     * @return 锁具信息
     */
    @Override
	public Lock selectLockById(Integer lockId)
	{
	    return lockMapper.selectLockById(lockId);
	}
	
	/**
     * 查询锁具列表
     * 
     * @param lock 锁具信息
     * @return 锁具集合
     */
	@Override
	public List<Lock> selectLockList(Lock lock)
	{
	    return lockMapper.selectLockList(lock);
	}
	
    /**
     * 新增锁具
     * 
     * @param lock 锁具信息
     * @return 结果
     */
	@Override
	public int insertLock(Lock lock)
	{
	    return lockMapper.insertLock(lock);
	}
	
	/**
     * 修改锁具
     * 
     * @param lock 锁具信息
     * @return 结果
     */
	@Override
	public int updateLock(Lock lock)
	{
	    return lockMapper.updateLock(lock);
	}

	/**
     * 删除锁具对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteLockByIds(String ids)
	{
		return lockMapper.deleteLockByIds(Convert.toStrArray(ids));
	}
	
}
