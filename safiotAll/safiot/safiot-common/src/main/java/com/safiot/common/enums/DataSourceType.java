package com.safiot.common.enums;

/**
 * 数据源
 * 
 * @author safiot
 */
public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
